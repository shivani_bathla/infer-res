Requirements: python3.x, matplotlib, numpy, seaborn

Usage: python src/plotResults.py 

The following parameters can be set in src/plotResults.py
<br />        1. timeout: in seconds (max=3600)
<br />        2. inferenceType: prior, posterior
<br />        3. benchmarkList: list of benchmarks to be analyzed. 
                             Choose from: BN_UAI, mastermind, blockmap, fs, 
                                     students, pedigree_uai, grid-BN, bnlearn
<br />        4. printQuery: Plot/data to be dumped. Options:
<br />    &emsp;        4.1 countWorkingTestcases: counts the number of working testcases 
                                          in the input benchmark
<br />    &emsp;        4.2 printErrMAR: prints the max-error, RMSE, runtimes obtained with 
                                FBP, TRWBP, WMB, IJGP, Gibbs, IBIA 
<br />    &emsp;        4.3 printPR: prints PR obtained with different methods. 
<br />    &emsp;        4.4 score_vs_runtime: plots the variation of SumScore with runtime constraint
<br />    &emsp;        4.5 histMaxError: plots the histogram of max-errors obtained with 
                                 different methods
<br />    &emsp;        4.6 histRMSE: plots the histogram of RMSE obtained with different methods
<br />    &emsp;        4.7 histPR: plots the histogram of error in PR (log(PR_ace)-log(PR_algo))
<br />    &emsp;        4.8 histMAR_aceNotWorking: plots the histogram of difference in marginals (MAR_IBIA-MAR_algo) for testcases where ACE doesn't work.

