from readMarginals import readMAR, readLibdaiMarginals, readAceMarginals, readIBIA
from statistics import mean
from readInput import uaiRead

import numpy as np
import os
import sys
import json
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pdb
import seaborn
import math
from collections import defaultdict

##############################################################
#################### Functions ###############################
##############################################################

## readEvidFile: Reads the evidence file and returns the evidence variables ##
def readEvidFile(evidFileName):
    evidenceDict={}
    evidLine = ""
    with open(evidFileName, 'r') as fid:
        for line in fid:
            line = line.strip()
            if(line!=""):
                evidLine = evidLine+' '+line
    parts = evidLine.split()
    numEvidVar = parts[0]
    for i in range(1,len(parts), 2):
        evidenceDict[int(parts[i])] = int(parts[i+1])
    return evidenceDict

## getCountInBins: returns count for each bin in the histogram
def getCountInBins(ax, binList):
    l4 = []
    for bar, b0, b1 in zip(ax.containers[0], binList[:-1], binList[1:]):
        l4.append(bar.get_height())
    return l4

## roundRuntime: rounds off runtime 
def roundRuntime(runtime):
    r = round(float(runtime))
    if(r==0):
        return '<1'
    else:
        return str(r)

## getRuntimeMerlin: reads the output log from Merlin and returns the runtime 
def getRuntimeMerlin(fileName):
    runtime_algo={}
    if (os.path.isfile(fileName)):
        with open(fileName) as fid:
            for line in fid:
                line = line.strip()
                if(line==""):
                    continue
                parts = line.split()
                if(parts[0]=='Network:'):
                    runtime_algo[parts[1]] = parts[-1]
    return runtime_algo

## getAvgKLD_bench: returns the average KL distance for a benchmark
def getAvgKLD_bench(marg_ace, marg_algo, nonEvidVar):
    # avgKld - over all states
    stateCount = 0
    avgKld = 0.0
    for v in nonEvidVar:
        p_ace= marg_ace[v]
        p_algo= marg_algo[v]
        # for each state
        for i in range(len(p_ace)):
            stateCount += 1
            if(p_ace[i]==0.0):
                # P(x)=0
                continue
            else:
                if(p_algo[i]==0.0):
                    # Q(x)=0 P(x)!=0
                    log_algo = -16
                else:
                    log_algo = np.log10(p_algo[i])

                # KLD = P(x)*log(P(x)/Q(x))
                log_ace = np.log10(p_ace[i])
                log_diff = log_ace-log_algo
                kld = p_ace[i]*log_diff
                avgKld += kld
    avgKld = round(avgKld/stateCount,5)
    if(avgKld<0):
        print('Negative kld error')
        sys.exit(1)
    return avgKld

## getStats: returns error statistics
def getStats(marg_algo, marg_ace, evidenceDict):
    maxErr, rmse = (0.0,0.0)
    # find error in marginals for all variable states
    err_algo = {v: np.round(np.abs(marg_algo[v]-marg_ace[v]),3) for v in marg_ace if v not in evidenceDict}

    numVar = len(err_algo)
    nonEvidVar = set(err_algo.keys())
    
    # RMSE, max-error
    for v in err_algo:
        maxErr = max(maxErr, np.max(err_algo[v]))
        rmse += np.mean(np.square(err_algo[v]))
    rmse = round(np.sqrt(rmse/numVar),3)

    # average KL distance
    avgKLd_bench = getAvgKLD_bench(marg_ace, marg_algo, nonEvidVar)

    return maxErr, rmse, avgKLd_bench

## checkErrors: checks if file exists and is not empty
def checkErrors(filePath):
    f = True
    errorMess=''
    if (not os.path.isfile(filePath)) or (os.path.getsize(filePath)==0):
        errorMess = 'error'
        f = False
    return f, errorMess

## getBenchmarkDetails: returns the list of benchmarks in each set; 
##                      depending on the query returns either all benchmarks or 
##                      those that run with ACE 
def getBenchmarkDetails(benchmark, inferenceType, printQuery):
    if(benchmark =='BN_UAI'):
        cktList_ace_post=['BN_0', 'BN_1', 'BN_10', 'BN_101', 'BN_103', 'BN_105', 'BN_107', 'BN_109', 'BN_11', 'BN_111', 'BN_113', 'BN_115', 'BN_117', 'BN_119', 'BN_121', 'BN_123', 'BN_125', 'BN_14', 'BN_2', 'BN_22', 'BN_23', 'BN_24', 'BN_25', 'BN_28', 'BN_29', 'BN_3', 'BN_30', 'BN_31', 'BN_32', 'BN_33', 'BN_34', 'BN_35', 'BN_36', 'BN_37', 'BN_38', 'BN_39', 'BN_4', 'BN_40', 'BN_41', 'BN_42', 'BN_43', 'BN_44', 'BN_45', 'BN_46', 'BN_47', 'BN_48', 'BN_49', 'BN_5', 'BN_50', 'BN_51', 'BN_52', 'BN_53', 'BN_54', 'BN_55', 'BN_56', 'BN_57', 'BN_58', 'BN_59', 'BN_6', 'BN_60', 'BN_61', 'BN_62', 'BN_63', 'BN_64', 'BN_65', 'BN_66', 'BN_67', 'BN_68', 'BN_7', 'BN_78', 'BN_79', 'BN_8', 'BN_80', 'BN_81', 'BN_82', 'BN_83', 'BN_84', 'BN_85', 'BN_86', 'BN_87', 'BN_88', 'BN_89', 'BN_9', 'BN_90', 'BN_91', 'BN_92', 'BN_93', 'BN_95', 'BN_97', 'BN_99']
        cktList_ace_prior=['BN_105', 'BN_107', 'BN_109', 'BN_111', 'BN_113', 'BN_115', 'BN_117', 'BN_119', 'BN_121', 'BN_123', 'BN_125', 'BN_20', 'BN_21', 'BN_22', 'BN_23', 'BN_24', 'BN_25', 'BN_26', 'BN_27', 'BN_28', 'BN_29', 'BN_30', 'BN_31', 'BN_32', 'BN_33', 'BN_34', 'BN_35', 'BN_36', 'BN_37', 'BN_38', 'BN_39', 'BN_40', 'BN_41', 'BN_42', 'BN_43', 'BN_44', 'BN_45', 'BN_46', 'BN_47', 'BN_48', 'BN_49', 'BN_50', 'BN_51', 'BN_52', 'BN_53', 'BN_54', 'BN_55', 'BN_56', 'BN_57', 'BN_58', 'BN_59', 'BN_60', 'BN_61', 'BN_62', 'BN_63', 'BN_64', 'BN_65', 'BN_66', 'BN_67', 'BN_68', 'BN_78', 'BN_79', 'BN_80', 'BN_81', 'BN_82', 'BN_83', 'BN_84', 'BN_85', 'BN_86', 'BN_87', 'BN_88', 'BN_89', 'BN_90', 'BN_91', 'BN_92', 'BN_93']
        cktList_aceNotWorking_post=['BN_12', 'BN_126', 'BN_127', 'BN_128', 'BN_129', 'BN_13', 'BN_130', 'BN_131', 'BN_132', 'BN_133', 'BN_134', 'BN_15', 'BN_16', 'BN_17', 'BN_18', 'BN_19', 'BN_20', 'BN_21', 'BN_26', 'BN_27', 'BN_69', 'BN_70', 'BN_71', 'BN_72', 'BN_73', 'BN_74', 'BN_75', 'BN_76', 'BN_77']
        cktList_aceNotWorking_prior=['BN_0', 'BN_1', 'BN_10', 'BN_101', 'BN_103', 'BN_11', 'BN_12', 'BN_126', 'BN_127', 'BN_128', 'BN_129', 'BN_13', 'BN_130', 'BN_131', 'BN_132', 'BN_133', 'BN_134', 'BN_14', 'BN_15', 'BN_16', 'BN_17', 'BN_18', 'BN_19', 'BN_2', 'BN_3', 'BN_4', 'BN_5', 'BN_6', 'BN_69', 'BN_7', 'BN_70', 'BN_71', 'BN_72', 'BN_73', 'BN_74', 'BN_75', 'BN_76', 'BN_77', 'BN_8', 'BN_9', 'BN_95', 'BN_97', 'BN_99']
        cktList_all=['BN_101', 'BN_103', 'BN_105', 'BN_107', 'BN_109', 'BN_111', 'BN_113', 'BN_115', 'BN_117', 'BN_119', 'BN_121', 'BN_123', 'BN_125', 'BN_126', 'BN_127', 'BN_128', 'BN_129', 'BN_130', 'BN_131', 'BN_132', 'BN_133', 'BN_134', 'BN_17', 'BN_19', 'BN_21', 'BN_23', 'BN_25', 'BN_27', 'BN_29', 'BN_31', 'BN_33', 'BN_35', 'BN_37', 'BN_39', 'BN_41', 'BN_48', 'BN_50', 'BN_52', 'BN_54', 'BN_56', 'BN_58', 'BN_60', 'BN_62', 'BN_64', 'BN_66', 'BN_68', 'BN_79', 'BN_81', 'BN_83', 'BN_85', 'BN_87', 'BN_89', 'BN_91', 'BN_93', 'BN_95', 'BN_97', 'BN_99', 'BN_0', 'BN_1', 'BN_10', 'BN_11', 'BN_14', 'BN_2', 'BN_28', 'BN_3', 'BN_30', 'BN_32', 'BN_34', 'BN_36', 'BN_38', 'BN_4', 'BN_40', 'BN_42', 'BN_43', 'BN_44', 'BN_45', 'BN_46', 'BN_47', 'BN_49', 'BN_5', 'BN_51', 'BN_53', 'BN_55', 'BN_57', 'BN_59', 'BN_6', 'BN_61', 'BN_63', 'BN_65', 'BN_67', 'BN_7', 'BN_78', 'BN_8', 'BN_80', 'BN_82', 'BN_84', 'BN_86', 'BN_88', 'BN_9', 'BN_90', 'BN_92', 'BN_22', 'BN_24', 'BN_12', 'BN_13', 'BN_15', 'BN_16', 'BN_20', 'BN_26', 'BN_69', 'BN_70', 'BN_71', 'BN_72', 'BN_73', 'BN_74', 'BN_75', 'BN_76', 'BN_77', 'BN_18']
        path_evid = './benchmarks/UAI/BN_UAI/'
        path_ace_post = './benchmarks/UAI/BN_UAI/ace.posteriorMarginals/'
        path_ace_prior = './benchmarks/UAI/BN_UAI/ace.priorMarginals/'
    else:
        if (benchmark=='mastermind'):
            cktList_ace_post=['mastermind_03_08_03-0000', 'mastermind_03_08_03-0001', 'mastermind_03_08_03-0002', 'mastermind_03_08_03-0003', 'mastermind_03_08_03-0004', 'mastermind_03_08_03-0005', 'mastermind_03_08_03-0006', 'mastermind_03_08_03-0007', 'mastermind_03_08_03-0008', 'mastermind_03_08_03-0009', 'mastermind_03_08_03-0010', 'mastermind_03_08_03-0011', 'mastermind_03_08_03-0012', 'mastermind_03_08_03-0013', 'mastermind_03_08_03-0014', 'mastermind_03_08_03-0015', 'mastermind_03_08_04-0000', 'mastermind_03_08_04-0001', 'mastermind_03_08_04-0002', 'mastermind_03_08_04-0003', 'mastermind_03_08_04-0004', 'mastermind_03_08_04-0005', 'mastermind_03_08_04-0006', 'mastermind_03_08_04-0007', 'mastermind_03_08_04-0008', 'mastermind_03_08_04-0009', 'mastermind_03_08_04-0010', 'mastermind_03_08_04-0011', 'mastermind_03_08_04-0012', 'mastermind_03_08_04-0013', 'mastermind_03_08_04-0014', 'mastermind_03_08_04-0015', 'mastermind_03_08_05-0000', 'mastermind_03_08_05-0001', 'mastermind_03_08_05-0002', 'mastermind_03_08_05-0003', 'mastermind_03_08_05-0004', 'mastermind_03_08_05-0005', 'mastermind_03_08_05-0006', 'mastermind_03_08_05-0007', 'mastermind_03_08_05-0008', 'mastermind_03_08_05-0009', 'mastermind_03_08_05-0010', 'mastermind_03_08_05-0011', 'mastermind_03_08_05-0012', 'mastermind_03_08_05-0013', 'mastermind_03_08_05-0014', 'mastermind_03_08_05-0015', 'mastermind_04_08_03-0000', 'mastermind_04_08_03-0001', 'mastermind_04_08_03-0002', 'mastermind_04_08_03-0003', 'mastermind_04_08_03-0004', 'mastermind_04_08_03-0005', 'mastermind_04_08_03-0006', 'mastermind_04_08_03-0007', 'mastermind_04_08_03-0008', 'mastermind_04_08_03-0009', 'mastermind_04_08_03-0010', 'mastermind_04_08_03-0011', 'mastermind_04_08_03-0012', 'mastermind_04_08_03-0013', 'mastermind_04_08_03-0014', 'mastermind_04_08_03-0015', 'mastermind_04_08_04-0000', 'mastermind_04_08_04-0001', 'mastermind_04_08_04-0002', 'mastermind_04_08_04-0003', 'mastermind_04_08_04-0004', 'mastermind_04_08_04-0005', 'mastermind_04_08_04-0006', 'mastermind_04_08_04-0007', 'mastermind_04_08_04-0008', 'mastermind_04_08_04-0009', 'mastermind_04_08_04-0010', 'mastermind_04_08_04-0011', 'mastermind_04_08_04-0012', 'mastermind_04_08_04-0013', 'mastermind_04_08_04-0014', 'mastermind_04_08_04-0015', 'mastermind_05_08_03-0000', 'mastermind_05_08_03-0001', 'mastermind_05_08_03-0002', 'mastermind_05_08_03-0003', 'mastermind_05_08_03-0004', 'mastermind_05_08_03-0005', 'mastermind_05_08_03-0006', 'mastermind_05_08_03-0007', 'mastermind_05_08_03-0008', 'mastermind_05_08_03-0009', 'mastermind_05_08_03-0010', 'mastermind_05_08_03-0011', 'mastermind_05_08_03-0012', 'mastermind_05_08_03-0013', 'mastermind_05_08_03-0014', 'mastermind_05_08_03-0015', 'mastermind_06_08_03-0000', 'mastermind_06_08_03-0001', 'mastermind_06_08_03-0002', 'mastermind_06_08_03-0003', 'mastermind_06_08_03-0004', 'mastermind_06_08_03-0005', 'mastermind_06_08_03-0006', 'mastermind_06_08_03-0007', 'mastermind_06_08_03-0008', 'mastermind_06_08_03-0009', 'mastermind_06_08_03-0010', 'mastermind_06_08_03-0011', 'mastermind_06_08_03-0012', 'mastermind_06_08_03-0013', 'mastermind_06_08_03-0014', 'mastermind_06_08_03-0015', 'mastermind_10_08_03-0000', 'mastermind_10_08_03-0001', 'mastermind_10_08_03-0002', 'mastermind_10_08_03-0003', 'mastermind_10_08_03-0004', 'mastermind_10_08_03-0005', 'mastermind_10_08_03-0006', 'mastermind_10_08_03-0007', 'mastermind_10_08_03-0008', 'mastermind_10_08_03-0009', 'mastermind_10_08_03-0010', 'mastermind_10_08_03-0011', 'mastermind_10_08_03-0012', 'mastermind_10_08_03-0013', 'mastermind_10_08_03-0014', 'mastermind_10_08_03-0015']
            cktList_ace_prior = cktList_ace_post
            cktList_all = cktList_ace_post
            path_evid='./benchmarks/UAI/relational/mastermind/'
            path_ace_post='./benchmarks/UAI/relational/mastermind/ace.posteriorMarginals/'
            path_ace_prior='./benchmarks/UAI/relational/mastermind/ace.priorMarginals/'
        elif (benchmark=='blockmap'):
            cktList_ace_post=['blockmap_05_01-0000', 'blockmap_05_01-0001', 'blockmap_05_01-0002', 'blockmap_05_01-0003', 'blockmap_05_01-0004', 'blockmap_05_01-0005', 'blockmap_05_01-0006', 'blockmap_05_01-0007', 'blockmap_05_01-0008', 'blockmap_05_01-0009', 'blockmap_05_01-0010', 'blockmap_05_01-0011', 'blockmap_05_01-0012', 'blockmap_05_01-0013', 'blockmap_05_01-0014', 'blockmap_05_01-0015', 'blockmap_05_02-0000', 'blockmap_05_02-0001', 'blockmap_05_02-0002', 'blockmap_05_02-0003', 'blockmap_05_02-0004', 'blockmap_05_02-0005', 'blockmap_05_02-0006', 'blockmap_05_02-0007', 'blockmap_05_02-0008', 'blockmap_05_02-0009', 'blockmap_05_02-0010', 'blockmap_05_02-0011', 'blockmap_05_02-0012', 'blockmap_05_02-0013', 'blockmap_05_02-0014', 'blockmap_05_02-0015', 'blockmap_05_03-0000', 'blockmap_05_03-0001', 'blockmap_05_03-0002', 'blockmap_05_03-0003', 'blockmap_05_03-0004', 'blockmap_05_03-0005', 'blockmap_05_03-0006', 'blockmap_05_03-0007', 'blockmap_05_03-0008', 'blockmap_05_03-0009', 'blockmap_05_03-0010', 'blockmap_05_03-0011', 'blockmap_05_03-0012', 'blockmap_05_03-0013', 'blockmap_05_03-0014', 'blockmap_05_03-0015', 'blockmap_10_01-0000', 'blockmap_10_01-0001', 'blockmap_10_01-0002', 'blockmap_10_01-0003', 'blockmap_10_01-0004', 'blockmap_10_01-0005', 'blockmap_10_01-0006', 'blockmap_10_01-0007', 'blockmap_10_01-0008', 'blockmap_10_01-0009', 'blockmap_10_01-0010', 'blockmap_10_01-0011', 'blockmap_10_01-0012', 'blockmap_10_01-0013', 'blockmap_10_01-0014', 'blockmap_10_01-0015', 'blockmap_10_02-0000', 'blockmap_10_02-0001', 'blockmap_10_02-0002', 'blockmap_10_02-0003', 'blockmap_10_02-0004', 'blockmap_10_02-0005', 'blockmap_10_02-0006', 'blockmap_10_02-0007', 'blockmap_10_02-0008', 'blockmap_10_02-0009', 'blockmap_10_02-0010', 'blockmap_10_02-0011', 'blockmap_10_02-0012', 'blockmap_10_02-0013', 'blockmap_10_02-0014', 'blockmap_10_02-0015', 'blockmap_10_03-0000', 'blockmap_10_03-0001', 'blockmap_10_03-0002', 'blockmap_10_03-0003', 'blockmap_10_03-0004', 'blockmap_10_03-0005', 'blockmap_10_03-0006', 'blockmap_10_03-0007', 'blockmap_10_03-0008', 'blockmap_10_03-0009', 'blockmap_10_03-0010', 'blockmap_10_03-0011', 'blockmap_10_03-0012', 'blockmap_10_03-0013', 'blockmap_10_03-0014', 'blockmap_10_03-0015', 'blockmap_15_01-0000', 'blockmap_15_01-0001', 'blockmap_15_01-0002', 'blockmap_15_01-0003', 'blockmap_15_01-0004', 'blockmap_15_01-0005', 'blockmap_15_01-0006', 'blockmap_15_01-0007', 'blockmap_15_01-0008', 'blockmap_15_01-0009', 'blockmap_15_01-0010', 'blockmap_15_01-0011', 'blockmap_15_01-0012', 'blockmap_15_01-0013', 'blockmap_15_01-0014', 'blockmap_15_01-0015', 'blockmap_15_02-0000', 'blockmap_15_02-0001', 'blockmap_15_02-0002', 'blockmap_15_02-0003', 'blockmap_15_02-0004', 'blockmap_15_02-0005', 'blockmap_15_02-0006', 'blockmap_15_02-0007', 'blockmap_15_02-0008', 'blockmap_15_02-0009', 'blockmap_15_02-0010', 'blockmap_15_02-0011', 'blockmap_15_02-0012', 'blockmap_15_02-0013', 'blockmap_15_02-0014', 'blockmap_15_02-0015', 'blockmap_15_03-0000', 'blockmap_15_03-0001', 'blockmap_15_03-0002', 'blockmap_15_03-0003', 'blockmap_15_03-0004', 'blockmap_15_03-0005', 'blockmap_15_03-0006', 'blockmap_15_03-0007', 'blockmap_15_03-0008', 'blockmap_15_03-0009', 'blockmap_15_03-0010', 'blockmap_15_03-0011', 'blockmap_15_03-0012', 'blockmap_15_03-0013', 'blockmap_15_03-0014', 'blockmap_15_03-0015', 'blockmap_20_01-0000', 'blockmap_20_01-0001', 'blockmap_20_01-0002', 'blockmap_20_01-0003', 'blockmap_20_01-0004', 'blockmap_20_01-0005', 'blockmap_20_01-0006', 'blockmap_20_01-0007', 'blockmap_20_01-0008', 'blockmap_20_01-0009', 'blockmap_20_01-0010', 'blockmap_20_01-0011', 'blockmap_20_01-0012', 'blockmap_20_01-0013', 'blockmap_20_01-0014', 'blockmap_20_01-0015', 'blockmap_20_02-0000', 'blockmap_20_02-0001', 'blockmap_20_02-0002', 'blockmap_20_02-0003', 'blockmap_20_02-0004', 'blockmap_20_02-0005', 'blockmap_20_02-0006', 'blockmap_20_02-0007', 'blockmap_20_02-0008', 'blockmap_20_02-0009', 'blockmap_20_02-0010', 'blockmap_20_02-0011', 'blockmap_20_02-0012', 'blockmap_20_02-0013', 'blockmap_20_02-0014', 'blockmap_20_02-0015', 'blockmap_20_03-0000', 'blockmap_20_03-0001', 'blockmap_20_03-0002', 'blockmap_20_03-0003', 'blockmap_20_03-0004', 'blockmap_20_03-0005', 'blockmap_20_03-0006', 'blockmap_20_03-0007', 'blockmap_20_03-0008', 'blockmap_20_03-0009', 'blockmap_20_03-0010', 'blockmap_20_03-0011', 'blockmap_20_03-0012', 'blockmap_20_03-0013', 'blockmap_20_03-0014', 'blockmap_20_03-0015', 'blockmap_22_01-0000', 'blockmap_22_01-0001', 'blockmap_22_01-0002', 'blockmap_22_01-0003', 'blockmap_22_01-0004', 'blockmap_22_01-0005', 'blockmap_22_01-0006', 'blockmap_22_01-0007', 'blockmap_22_01-0008', 'blockmap_22_01-0009', 'blockmap_22_01-0010', 'blockmap_22_01-0011', 'blockmap_22_01-0012', 'blockmap_22_01-0013', 'blockmap_22_01-0014', 'blockmap_22_01-0015', 'blockmap_22_02-0000', 'blockmap_22_02-0001', 'blockmap_22_02-0002', 'blockmap_22_02-0003', 'blockmap_22_02-0004', 'blockmap_22_02-0005', 'blockmap_22_02-0006', 'blockmap_22_02-0007', 'blockmap_22_02-0008', 'blockmap_22_02-0009', 'blockmap_22_02-0010', 'blockmap_22_02-0011', 'blockmap_22_02-0012', 'blockmap_22_02-0013', 'blockmap_22_02-0014', 'blockmap_22_02-0015', 'blockmap_22_03-0000', 'blockmap_22_03-0001', 'blockmap_22_03-0002', 'blockmap_22_03-0003', 'blockmap_22_03-0004', 'blockmap_22_03-0005', 'blockmap_22_03-0006', 'blockmap_22_03-0007', 'blockmap_22_03-0008', 'blockmap_22_03-0009', 'blockmap_22_03-0010', 'blockmap_22_03-0011', 'blockmap_22_03-0012', 'blockmap_22_03-0013', 'blockmap_22_03-0014', 'blockmap_22_03-0015']
            cktList_ace_prior = cktList_ace_post
            cktList_all = cktList_ace_post
            path_evid='./benchmarks/UAI/relational/blockmap/'
            path_ace_post='./benchmarks/UAI/relational/blockmap/ace.posteriorMarginals/'
            path_ace_prior='./benchmarks/UAI/relational/blockmap/ace.priorMarginals/'
            
        elif(benchmark=='students'):
            cktList_ace_post=['students_03_02-0000', 'students_03_02-0001', 'students_03_02-0002', 'students_03_02-0003', 'students_03_02-0004', 'students_03_02-0005', 'students_03_02-0006', 'students_03_02-0007', 'students_03_02-0008', 'students_03_02-0009', 'students_03_02-0010', 'students_03_02-0011', 'students_03_02-0012', 'students_03_02-0013', 'students_03_02-0014', 'students_03_02-0015']
            cktList_ace_prior = cktList_ace_post
            cktList_all = cktList_ace_post
            path_evid='./benchmarks/UAI/relational/students/'
            path_ace_post='./benchmarks/UAI/relational/students/ace.posteriorMarginals/'
            path_ace_prior='./benchmarks/UAI/relational/students/ace.priorMarginals/'

        elif(benchmark=='pedigree_uai'):
            cktList_ace_post=['pedigree18', 'pedigree1', 'pedigree20', 'pedigree23', 'pedigree30', 'pedigree33', 'pedigree37', 'pedigree38', 'pedigree39']
            cktList_aceNotWorking_post = ['pedigree13','pedigree19', 'pedigree25', 'pedigree31', 'pedigree34', 'pedigree40', 'pedigree41', 'pedigree42', 'pedigree44', 'pedigree50', 'pedigree51', 'pedigree7', 'pedigree9']
            cktList_all = ['pedigree13', 'pedigree18', 'pedigree19', 'pedigree1', 'pedigree20', 'pedigree23', 'pedigree25', 'pedigree30', 'pedigree31', 'pedigree33', 'pedigree34', 'pedigree37', 'pedigree38', 'pedigree39', 'pedigree40', 'pedigree41', 'pedigree42', 'pedigree44', 'pedigree50', 'pedigree51', 'pedigree7', 'pedigree9']
            cktList_ace_prior = []
            path_evid='./benchmarks/UAI/pedigree_uai/'
            path_ace_post='./benchmarks/UAI/pedigree_uai/ace.posteriorMarginals/'
            path_ace_prior='./benchmarks/UAI/pedigree_uai/ace.priorMarginals/'

        elif(benchmark=='fs'):
            cktList_ace_post=['fs-01', 'fs-04', 'fs-07', 'fs-10', 'fs-13', 'fs-16', 'fs-19', 'fs-22', 'fs-25', 'fs-28', 'fs-29']
            cktList_ace_prior=['fs-01', 'fs-04']
            cktList_all = cktList_ace_post
            path_evid='./benchmarks/UAI/relational/fs/'
            path_ace_post='./benchmarks/UAI/relational/fs/ace.posteriorMarginals/'
            path_ace_prior='./benchmarks/UAI/relational/fs/ace.priorMarginals/'
        elif(benchmark=='grid-BN'):
            cktList_all=['50-12-5', '50-14-5', '50-15-5', '50-16-5', '50-17-5', '50-18-5', '50-19-5', '50-20-5', '75-16-5', '75-17-5', '75-18-5', '75-19-5', '75-20-5', '75-21-5', '75-22-5', '75-23-5', '75-24-5', '75-25-5', '75-26-5', '90-20-5', '90-21-5', '90-22-5', '90-23-5', '90-24-5', '90-25-5', '90-26-5', '90-30-5', '90-34-5', '90-38-5', '90-42-5', '90-46-5', '90-50-5']
            cktList_ace_prior=['50-12-5', '50-14-5', '50-15-5', '50-16-5', '50-17-5', '50-18-5', '50-19-5', '75-16-5', '75-17-5', '75-18-5', '75-19-5', '75-20-5', '75-21-5', '75-22-5', '75-23-5', '75-24-5', '75-26-5', '90-20-5', '90-21-5', '90-22-5', '90-23-5', '90-24-5', '90-25-5', '90-26-5', '90-30-5', '90-34-5', '90-38-5', '90-42-5', '90-46-5']
            cktList_ace_post = []
            path_evid=''
            path_ace_post=''
            path_ace_prior='./benchmarks/grid-BN/ace.priorMarginals/'
            
        elif(benchmark=='bnlearn'):
            cktList_all=['alarm', 'andes', 'asia', 'barley', 'cancer', 'child', 'diabetes', 'earthquake', 'hailfinder', 'hepar2', 'insurance', 'link', 'mildew', 'munin1', 'munin2', 'munin3', 'munin4', 'munin', 'pathfinder', 'pigs', 'sachs', 'sprinkler', 'student1', 'survey', 'water', 'win95pts']
            cktList_ace_prior=cktList_all
            cktList_ace_post = []
            path_evid=''
            path_ace_post=''
            path_ace_prior='./benchmarks/bnlearn/ace.priorMarginals/'
        else:
            print('Incorrect benchmark name', benchmark)
            sys.exit(1)
    # return set depending on printQuery and inference type
    if(inferenceType=='posterior'):
        if(printQuery=='histMAR_aceNotWorking'):
            return cktList_aceNotWorking_post, path_evid, path_ace_post
        elif(printQuery=='countWorkingTestcases') or (printQuery=='printPR'):
            return cktList_all, path_evid, path_ace_post
        else:
            return cktList_ace_post, path_evid, path_ace_post
    else:
        if(printQuery=='histMAR_aceNotWorking'):
            return cktList_aceNotWorking_prior, path_evid, path_ace_prior
        elif(printQuery=='countWorkingTestcases') or (printQuery=='printPR'):
            return cktList_all, path_evid, path_ace_prior
        else:
            return cktList_ace_prior, path_evid, path_ace_prior

def getPaths(benchmark, inferenceType):
    path_wmb = './results/merlin/'+benchmark+'.'+inferenceType+'.wmb.ibound20.iter10/'
    path_ijgp = './results/merlin/'+benchmark+'.'+inferenceType+'.ijgp.ibound20.iter10/'
    path_wmb10 = './results/merlin/'+benchmark+'.'+inferenceType+'.wmb.ibound10.iter10/'
    path_ijgp10 = './results/merlin/'+benchmark+'.'+inferenceType+'.ijgp.ibound10.iter10/'
    path_fbp = './results/libDAI/'+benchmark+'.'+inferenceType+'.fbp/'
    path_trwbp = './results/libDAI/'+benchmark+'.'+inferenceType+'.trwbp/'
    path_gibbs = './results/libDAI/'+benchmark+'.'+inferenceType+'.gibbs/'
    path_ibia = './results/IBIA/'+benchmark+'.'+inferenceType+'.ibia/'
    resFile_wmb = path_wmb+'results.'+inferenceType+'.'+benchmark+'.wmb.ibound20.iter10'
    resFile_ijgp = path_ijgp+'results.'+inferenceType+'.'+benchmark+'.ijgp.ibound20.iter10'
    resFile_wmb10 = path_wmb10+'results.'+inferenceType+'.'+benchmark+'.wmb.ibound10.iter10'
    resFile_ijgp10 = path_ijgp10+'results.'+inferenceType+'.'+benchmark+'.ijgp.ibound10.iter10'
    return path_wmb, path_ijgp, path_fbp, path_trwbp, path_gibbs, path_ibia, resFile_wmb, resFile_ijgp, path_wmb10, path_ijgp10,resFile_wmb10, resFile_ijgp10

def getData(timeout, benchmarkList, printQuery, inferenceType):

    prHistData = [[],[],[], []]
    kldData = [[[],[]], [[],[]], [[],[]], [[],[]], [[],[]], [[],[]], [[],[]]]
    histMAR_noAce=[[],[],[]]
    histMaxError=[[],[],[],[],[], []]
    histRMSE=[[],[],[],[],[], []]
    (count_fbp,count_trwbp,count_wmb, count_ijgp,count_gibbs, count_wmb10, count_ibia) = [0]*7
    totalNumBenchmarks = 0
    numBench_histMAR = 0
    printStr_MAR=''
    printStr_PR=''
    
    for benchmark in benchmarkList:
        cktList, path_evid, path_ace = getBenchmarkDetails(benchmark, inferenceType, printQuery)
        path_wmb, path_ijgp, path_fbp, path_trwbp, path_gibbs, path_ibia, resFile_wmb, resFile_ijgp, path_wmb10, path_ijgp10,resFile_wmb10, resFile_ijgp10  = getPaths(benchmark, inferenceType)
    
        # read runtime from results file for wmb, ijgp
        runtime_wmb = getRuntimeMerlin(resFile_wmb)
        runtime_wmb10 = getRuntimeMerlin(resFile_wmb10)
        runtime_ijgp = getRuntimeMerlin(resFile_ijgp)
        
        # Read marginal file for different algorithms
        for ckt in cktList:
            printStr_MAR += ckt
            printStr_PR += ckt
            totalNumBenchmarks += 1
            # Output files for each method
            aceFile = path_ace+ckt+'.uai.marginals'
            wmbFile = path_wmb+ckt+'.uai.MAR'
            ijgpFile = path_ijgp+ckt+'.uai.MAR'
            wmb10File = path_wmb10+ckt+'.uai.MAR'
            ijgp10File = path_ijgp10+ckt+'.uai.MAR'
            fbpFile = path_fbp+ckt+'.fbp.marginals'
            trwbpFile = path_trwbp+ckt+'.trwbp.marginals'
            gibbsFile = path_gibbs+ckt+'.gibbs.marginals'
            ibiaFile = path_ibia+ckt+'.mcs20.15.withCollapse.withPrune.MAR'
            evidFile = path_evid+ckt+'.uai.evid'
            # Read evidence file
            if(inferenceType=='posterior'):
                evidenceDict = readEvidFile(evidFile)
            else:
                evidenceDict = {}
            # for pedigree: evidence is built into the network
            if(benchmark=='pedigree_uai'):
                uaiFile = path_evid+ckt+'.uai'
                evidIncFlag, evidenceDict = uaiRead(uaiFile, 'posterior')

            # testcases that work with mcs_im=10
            if((inferenceType=='posterior') and (ckt in ['BN_71','BN_74','BN_77','pedigree38','pedigree40'])) or((inferenceType=='prior') and (ckt in ['BN_103', 'BN_97', 'BN_2'])):
                ibiaFile = path_ibia+ckt+'.mcs20.10.withCollapse.withPrune.MAR'

            # check existence of output files
            f_ace, err_ace = checkErrors(aceFile)
            f_wmb, err_wmb = checkErrors(wmbFile)
            f_ijgp, err_ijgp = checkErrors(ijgpFile)
            f_fbp, err_fbp = checkErrors(fbpFile)
            f_trwbp, err_trwbp = checkErrors(trwbpFile)
            f_gibbs, err_gibbs = checkErrors(gibbsFile)
            f_ibia, err_ibia = checkErrors(ibiaFile)
            f_wmb10, err_wmb10 = checkErrors(wmb10File)
        
            ### ACE ###
            if(f_ace):
                marg_ace, runtime_ace, pr_ace = readAceMarginals(aceFile)
                printStr_PR += ','+str(round(pr_ace,2))
            else:
                printStr_PR += ',err'

        
            ### FBP ###
            errFlag_fbp = False
            if(f_fbp):
                # read output
                marg_fbp, runtime_fbp, numIter_fbp, pr_fbp = readLibdaiMarginals(fbpFile, 'fbp')
                r = 0 if float(runtime_fbp)<1 else round(float(runtime_fbp))
                if(pr_fbp=='nan'):
                    err_fbp='Err: Incomplete run or PR=nan'
                    errFlag_fbp = True
                elif (r>timeout): # runtime>timeout
                    err_fbp='Err: Timeout'
                    errFlag_fbp = True
                elif(f_ace):
                    # find Delta_PR
                    delta_pr_fbp = "{:.2e}".format(pr_fbp-pr_ace)
                    prHistData[0].append(float(delta_pr_fbp))
                    # find error stats for marginals
                    maxErr_fbp, rmse_fbp, avgKLd_fbp = getStats(marg_fbp, marg_ace, evidenceDict)
                    histMaxError[0].append(maxErr_fbp)
                    histRMSE[0].append(rmse_fbp)
                    printStr_MAR += ','+str(maxErr_fbp)+','+str(rmse_fbp)+','+roundRuntime(runtime_fbp)
                    kldData[0][0].append(runtime_fbp)
                    kldData[0][1].append(avgKLd_fbp)
            else:
                errFlag_fbp = True
            if (errFlag_fbp):
                printStr_MAR += ',err,err,err'
                printStr_PR += ',err'
            else:
                # count number of working testcases
                count_fbp += 1
                printStr_PR += ','+str(round(pr_fbp,2))
                  

            ### TRWBP ###
            errFlag_trwbp = False
            if(f_trwbp):
                # read output
                marg_trwbp, runtime_trwbp, numIter_trwbp, pr_trwbp = readLibdaiMarginals(trwbpFile, 'trwbp')
                r = 0 if float(runtime_trwbp)<1 else round(float(runtime_trwbp))
                if(pr_trwbp=='nan'):
                    err_trwbp='Err: Incomplete run or PR=nan'
                    errFlag_trwbp = True
                elif (r>timeout): # runtime>timeout
                    err_trwbp='Err: Timeout'
                    errFlag_trwbp = True
                elif(f_ace):
                    # find Delta_PR
                    delta_pr_trwbp = "{:.2e}".format(pr_trwbp-pr_ace)
                    prHistData[1].append(float(delta_pr_trwbp))
                    # find error stats for marginals
                    maxErr_trwbp, rmse_trwbp, avgKLd_trwbp = getStats(marg_trwbp, marg_ace, evidenceDict)
                    histMaxError[1].append(maxErr_trwbp)
                    histRMSE[1].append(rmse_trwbp)
                    printStr_MAR += ','+str(maxErr_trwbp)+','+str(rmse_trwbp)+','+roundRuntime(runtime_trwbp)
                    kldData[1][0].append(runtime_trwbp)
                    kldData[1][1].append(avgKLd_trwbp)
            else:
                errFlag_trwbp = True
            if (errFlag_trwbp):
                printStr_MAR += ',err,err,err'
                printStr_PR += ',err'
            else:
                # count number of working testcases
                count_trwbp += 1
                printStr_PR += ','+str(round(pr_trwbp,2))

        
            ### WMB ###
            pr = False
            errFlag_wmb = False
            if(f_wmb):
                nv_wmb, marg_wmb, pr_wmb, status_wmb = readMAR(wmbFile)
                r = 0 if float(runtime_wmb[ckt])<1 else round(float(runtime_wmb[ckt]))
                if (status_wmb[0]=='false'): # status: false
                    err_wmb = 'Err:'+status_wmb[1]
                    errFlag_wmb = True
                elif (r>timeout): # runtime>timeout
                    err_wmb='Err: Timeout'
                    errFlag_wmb = True
                elif (status_wmb[0]=='true') and f_ace:
                    # find Delta_PR
                    delta_pr_wmb = "{:.2e}".format(pr_wmb-pr_ace)
                    prHistData[2].append(float(delta_pr_wmb))
                    # find error stats for marginals
                    maxErr_wmb, rmse_wmb, avgKLd_wmb = getStats(marg_wmb, marg_ace, evidenceDict)
                    histMaxError[2].append(maxErr_wmb)
                    histRMSE[2].append(rmse_wmb)
                    printStr_MAR += ','+str(maxErr_wmb)+','+str(rmse_wmb)+','+roundRuntime(runtime_wmb[ckt])
                    kldData[2][0].append(float(runtime_wmb[ckt]))
                    kldData[2][1].append(avgKLd_wmb)
            else: 
                errFlag_wmb = True
            if (errFlag_wmb):
                printStr_MAR += ',err,err,err'
                printStr_PR += ',err'
            else:
                # count number of working testcases
                count_wmb += 1
                printStr_PR += ','+str(round(pr_wmb,2))

        
            ### IJGP ###
            errFlag_ijgp = False
            if(f_ijgp):
                nv_ijgp, marg_ijgp, pr_ijgp, status_ijgp = readMAR(ijgpFile)
                r = 0 if float(runtime_ijgp[ckt])<1 else round(float(runtime_ijgp[ckt]))
                if (status_ijgp[0]=='false'): # status: false
                    err_ijgp = 'Err:'+status_ijgp[1]
                    errFlag_ijgp = True
                elif (r>timeout): # runtime>timeout
                    err_ijgp='Err: Timeout'
                    errFlag_ijgp = True
                elif (status_ijgp[0]=='true') and (f_ace):
                    # find error stats for marginals
                    maxErr_ijgp, rmse_ijgp, avgKLd_ijgp = getStats(marg_ijgp, marg_ace, evidenceDict)
                    histMaxError[3].append(maxErr_ijgp)
                    histRMSE[3].append(rmse_ijgp)
                    printStr_MAR += ','+str(maxErr_ijgp)+','+str(rmse_ijgp)+','+roundRuntime(runtime_ijgp[ckt])
                    kldData[3][0].append(float(runtime_ijgp[ckt]))
                    kldData[3][1].append(avgKLd_ijgp)
            else:
                errFlag_ijgp = True
            if (errFlag_ijgp):
                printStr_MAR += ',err,err,err'
            else:
                # count number of working testcases
                count_ijgp += 1
       
            ### GIBBS ###
            errFlag_gibbs = False
            if(f_gibbs):
                marg_gibbs, runtime_gibbs, numIter_gibbs, pr_gibbs = readLibdaiMarginals(gibbsFile, 'gibbs')
                r = 0 if float(runtime_gibbs)<1 else round(float(runtime_gibbs))
                if (r>timeout): # runtime>timeout
                    err_gibbs='Err: Timeout'
                    errFlag_gibbs = True
                elif(f_ace):
                    # find error stats for marginals
                    maxErr_gibbs, rmse_gibbs, avgKLd_gibbs = getStats(marg_gibbs, marg_ace, evidenceDict)
                    histMaxError[4].append(maxErr_gibbs)
                    histRMSE[4].append(rmse_gibbs)
                    printStr_MAR += ','+str(maxErr_gibbs)+','+str(rmse_gibbs)+','+roundRuntime(runtime_gibbs)
                    kldData[4][0].append(runtime_gibbs)
                    kldData[4][1].append(avgKLd_gibbs)
            else:
                errFlag_gibbs = True
            if (errFlag_gibbs):
                printStr_MAR += ',err,err,err'
            else:
                # count number of working testcases
                count_gibbs += 1

            ### WMB10 ###
            errFlag_wmb10 = False
            if(f_wmb10):
                nv_wmb10, marg_wmb10, pr_wmb10, status_wmb10 = readMAR(wmb10File)
                r = 0 if float(runtime_wmb10[ckt])<1 else round(float(runtime_wmb10[ckt]))
                if (status_wmb10[0]=='false'): # status: false
                    err_wmb10 = 'Err:'+status_wmb10[1]
                    errFlag_wmb10 = True
                elif (r>timeout): # runtime>timeout
                    err_wmb10='Err: Timeout'
                    errFlag_wmb10 = True
                elif (status_wmb10[0]=='true') and (f_ace):
                    delta_pr_wmb10 = "{:.2e}".format(pr_wmb10-pr_ace)
                    maxErr_wmb10, rmse_wmb10, avgKLd_wmb10 = getStats(marg_wmb10, marg_ace, evidenceDict)
                    kldData[5][0].append(float(runtime_wmb10[ckt]))
                    kldData[5][1].append(avgKLd_wmb10)
            else:
                errFlag_wmb10 = True
            # count number of working testcases
            if(not errFlag_wmb10):
                count_wmb10 += 1



            ### IBIA ###
            errFlag_ibia = False
            if(f_ibia):
                marg_ibia, pr_ibia, pr_runtime_ibia, runtime_ibia = readIBIA(ibiaFile)
                r = 0 if float(runtime_ibia)<1 else round(float(runtime_ibia))
                if (r>timeout): # runtime>timeout
                    err_ibia='Err: Timeout'
                    errFlag_ibia = True
                elif(f_ace):
                    delta_pr_ibia = "{:.2e}".format(float(pr_ibia)-pr_ace)
                    prHistData[3].append(float(delta_pr_ibia))
                    # find error stats for marginals
                    maxErr_ibia, rmse_ibia, avgKLd_ibia = getStats(marg_ibia, marg_ace, evidenceDict)
                    printStr_MAR += ','+str(maxErr_ibia)+','+str(rmse_ibia)+','+roundRuntime(runtime_ibia)+'\n'
                    histMaxError[5].append(maxErr_ibia)
                    histRMSE[5].append(rmse_ibia)
                    kldData[6][0].append(float(runtime_ibia))
                    kldData[6][1].append(avgKLd_ibia)
            else:
                errFlag_ibia = True

            if(not errFlag_ibia):
                # count number of working testcases
                count_ibia += 1
                printStr_PR += ','+str(round(float(pr_ibia),2))+'\n'
                if(float(runtime_ibia) <= timeout):
                    # form histogram for cases where ACE does not work
                    #if (errFlag_fbp  or errFlag_ijgp):
                    if (errFlag_fbp or errFlag_wmb or errFlag_ijgp):
                        continue
                    numBench_histMAR += 1
                    nonEvidVar = set(marg_ibia.keys()).difference(set(evidenceDict))
                    # check if fbp works
                    if(not errFlag_fbp):
                        diffMAR = [marg_ibia[v][i]-marg_fbp[v][i] for v in nonEvidVar for i in range(len(marg_ibia[v]))]
                        histMAR_noAce[0] += diffMAR
                    # check if wmb works
                    if(not errFlag_wmb):
                        diffMAR = [marg_ibia[v][i]-marg_wmb[v][i] for v in nonEvidVar for i in range(len(marg_ibia[v]))]
                        histMAR_noAce[1] += diffMAR
                    # check if ijgp works
                    if(not errFlag_ijgp):
                        diffMAR = [marg_ibia[v][i]-marg_ijgp[v][i] for v in nonEvidVar for i in range(len(marg_ibia[v]))]
                        histMAR_noAce[2] += diffMAR
            else:
                printStr_MAR += ',err,err,err\n'
                printStr_PR += ',err\n'


    #### Score plots ###
    if(printQuery=='score_vs_runtime'):
        numPoints = 5
        algoList=['FBP', 'TRWBP', 'WMB', 'IJGP', 'Gibbs', 'WMB10', 'IBIA']
        numBList= [0] * 7
        plotData = [[], [], [], [], [],[], []]
        maxRuntime = min(max([max(kldData[i][0]) for i in range(7) if(len(kldData[i][0]))]), 3600)
        
        # find score for each timeout constraint
        for timeout in np.arange(maxRuntime/numPoints, maxRuntime+1, maxRuntime/numPoints):
            print('Algo, Score, Timeout, #Benchmarks, TotalBenchmarks')
            for i in range(7):
                avgKLD = 0.0
                numB = 0
                sumScore = 0
                # find score and number of working testcases
                for j in range(len(kldData[i][0])):
                    r = kldData[i][0][j]
                    kld = kldData[i][1][j]
                    if(r<=timeout):
                        sumScore += 10**(-kld)
                        numB += 1
                numBList[i]=numB
                print(algoList[i], round(sumScore,2), round(timeout), numB, totalNumBenchmarks, sep=',')
                plotData[i].append((timeout,sumScore))
            print()
        # PLOT
        ax = plt.subplot(111)
        ax.plot(*zip(*plotData[6]), color='r', marker='s', linestyle='dashed', label='IBIA', markersize=15, markerfacecolor='none', markeredgewidth=2)
        ax.plot(*zip(*plotData[1]), color='black', marker='^',linestyle='dashed', label='TRWBP', markersize=7, markeredgewidth=2)
        ax.plot(*zip(*plotData[2]), color='blue', marker='P',  linestyle='dashed', label='WMB', markersize=10)
        ax.plot(*zip(*plotData[5]), color='darkgoldenrod', marker='+',  linestyle='dashed', label='WMB10', markersize=10, markeredgewidth=3)
        ax.plot(*zip(*plotData[3]), color='magenta', marker='*', linestyle='dashed', label='IJGP', markersize=18, markerfacecolor='none', markeredgewidth=1.5)
        ax.plot(*zip(*plotData[0]), color='g', marker='o', markerfacecolor='none', markersize=13, linestyle='dashed', label='FBP', markeredgewidth=2)
        ax.plot(*zip(*plotData[4]), color='deepskyblue', marker='d', linestyle='dashed', label='GIBBS', markersize=12, markerfacecolor='none', markeredgewidth=2)


        ax.set_xlabel('Runtime constraint (s)', fontsize=15)
        ax.set_ylabel('SumScore', fontsize=15)
        ax.set_xlim(0, int(maxRuntime+maxRuntime/5))
        box = ax.get_position()
        ax.set_position([box.x0, box.y0,
                 box.width*0.8, box.height])
        ax.legend(loc='upper left', bbox_to_anchor=(1, 1),
          fancybox=True, edgecolor='k')
        
        plt.show()

    ### Historgram of DeltaPR ###
    elif(printQuery=='histPR'):
    
        binwidth = 1.0
        err_min = -10.5
        err_max = 10.5
        binList = np.arange(err_min, err_max+2*binwidth, step=binwidth)

        ## PLOT
        fig, (ax1,ax2,ax3,ax4) = plt.subplots(1,4, sharey=True)
        (n1, bins, patches) = ax1.hist(prHistData[0], color='green', bins=binList, edgecolor='black', linewidth=1)
        ax1.set_title('FBP', fontsize=15)
        ax1.set_ylabel('#Instances', fontsize=15)
        ax1.set_xlabel(r'$\Delta_{FBP}$', fontsize=15)
        (n2, bins, patches) = ax2.hist(prHistData[1], color='black', bins=binList, edgecolor='black', linewidth=1)
        ax2.set_title('TRWBP', fontsize=15)
        ax2.set_xlabel(r'$\Delta_{TRWBP}$', fontsize=15)
        (n3, bins, patches) = ax3.hist(prHistData[2], color='blue', bins=binList, edgecolor='black', linewidth=1)
        ax3.set_title('WMB', fontsize=15)
        ax3.set_xlabel(r'$\Delta_{WMB}$', fontsize=15)
        (n4, bins, patches) = ax4.hist(prHistData[3], color='red', bins=binList, edgecolor='black', linewidth=1)
        ax4.set_title('IBIA', fontsize=15)
        ax4.set_xlabel(r'$\Delta_{IBIA}$', fontsize=15)
        ax1.set_ylim(0, totalNumBenchmarks)
        ax2.set_ylim(0, totalNumBenchmarks)
        ax3.set_ylim(0, totalNumBenchmarks)
        ax4.set_ylim(0, totalNumBenchmarks)
        print('FBP - binCount:',n1, 'total:', len(prHistData[0]))
        print('TRWBP - binCount:',n2, 'total:', len(prHistData[1]))
        print('WMB - binCount:',n3, 'total:', len(prHistData[2]))
        print('IBIA - binCount:',n4, 'total:', len(prHistData[3]))
        plt.show()

    ### Histogram of MAR for cases where ace doesn't work ###
    elif(printQuery=='histMAR_aceNotWorking'):
        binList = np.arange(-0.21,0.21,step=0.02)
        #binList = np.arange(-1.025,1.025,step=0.05)
        print('bin list:',binList)

        ## PLOT
        fig, (ax1,ax2,ax3) =  plt.subplots(1,3, sharey=True)
        seaborn.histplot(ax=ax1, data=histMAR_noAce[0], bins=binList, stat='percent', color='green', edgecolor='black', linewidth=1)
        ax1.set_title('FBP', fontsize=15)
        ax1.set_ylabel('Percentage of states (%)', fontsize=15)
        ax1.set_xlabel(r'$\Delta_{MAR}$', fontsize=15)
        seaborn.histplot(ax=ax2, data=histMAR_noAce[1], bins=binList, stat='percent', color='blue', edgecolor='black', linewidth=1)
        ax2.set_title('WMB', fontsize=15)
        ax2.set_xlabel(r'$\Delta_{MAR}$', fontsize=15)
        seaborn.histplot(ax=ax3, data=histMAR_noAce[2], bins=binList, stat='percent', color='magenta', edgecolor='black', linewidth=1)
        ax3.set_title('IJGP', fontsize=15)
        ax3.set_xlabel(r'$\Delta_{MAR}$', fontsize=15)
        print('number of testcases', numBench_histMAR)
        print('FBP', np.min(histMAR_noAce[0]), np.max(histMAR_noAce[0]))
        print('WMB', np.min(histMAR_noAce[1]), np.max(histMAR_noAce[1]))
        print('IJGP', np.min(histMAR_noAce[2]), np.max(histMAR_noAce[2]))
        
        #fig, (ax1,ax2) =  plt.subplots(1,2, sharey=True)
        #seaborn.histplot(ax=ax1, data=histMAR_noAce[0], bins=binList, stat='percent', color='green', edgecolor='black', linewidth=1)
        #ax1.set_title('FBP', fontsize=15)
        #ax1.set_ylabel('Percentage of states (%)', fontsize=15)
        #ax1.set_xlabel(r'$\Delta_{MAR}$', fontsize=15)
        #seaborn.histplot(ax=ax2, data=histMAR_noAce[2], bins=binList, stat='percent', color='magenta', edgecolor='black', linewidth=1)
        #ax2.set_title('IJGP', fontsize=15)
        #ax2.set_xlabel(r'$\Delta_{MAR}$', fontsize=15)
        plt.show()

    ### Historgram of maximum error ###
    elif(printQuery=='histMaxError'):
        binList = np.linspace(0,1,21)
        print('bins', binList)

        #PLOT
        fig, (ax1,ax2,ax3,ax4,ax5,ax6) = plt.subplots(1,6, sharey=True)
        seaborn.histplot(ax=ax1, data=histMaxError[0], bins=binList, stat='count', color='green', edgecolor='black', linewidth=1)
        ax1.set_title('FBP', fontsize=15)
        ax1.set_ylabel('#Instances', fontsize=15)
        ax1.set_xlabel('Maximum Error', fontsize=15)
        print('FBP - binCount:',getCountInBins(ax1, binList), 'total:', len(histMaxError[0]))
        seaborn.histplot(ax=ax2, data=histMaxError[1], bins=binList, stat='count', color='black', edgecolor='black', linewidth=1)
        ax2.set_title('TRWBP', fontsize=15)
        ax2.set_xlabel('Maximum Error', fontsize=15)
        print('TRWBP - binCount:',getCountInBins(ax2, binList), 'total:', len(histMaxError[1]))
        seaborn.histplot(ax=ax3, data=histMaxError[2], bins=binList, stat='count', color='blue', edgecolor='black', linewidth=1)
        ax3.set_title('WMB', fontsize=15)
        ax3.set_xlabel('Maximum Error', fontsize=15)
        print('WMB - binCount:',getCountInBins(ax3, binList), 'total:', len(histMaxError[2]))
        seaborn.histplot(ax=ax4, data=histMaxError[3], bins=binList, stat='count', color='magenta', edgecolor='black', linewidth=1)
        ax4.set_title('IJGP', fontsize=15)
        ax4.set_xlabel('Maximum Error', fontsize=15)
        print('IJGP - binCount:',getCountInBins(ax4, binList), 'total:', len(histMaxError[3]))
        seaborn.histplot(ax=ax5, data=histMaxError[4], bins=binList, stat='count', color='deepskyblue', edgecolor='black', linewidth=1)
        ax5.set_title('GIBBS', fontsize=15)
        ax5.set_xlabel('Maximum Error', fontsize=15)
        print('Gibbs - binCount:',getCountInBins(ax5, binList), 'total:', len(histMaxError[4]))
        seaborn.histplot(ax=ax6, data=histMaxError[5], bins=binList, stat='count', color='red', edgecolor='black', linewidth=1)
        ax6.set_title('IBIA', fontsize=15)
        ax6.set_xlabel('Maximum Error', fontsize=15)
        print('IBIA - binCount:',getCountInBins(ax6, binList), 'total:', len(histMaxError[5]))
        plt.show()
    ### Historgram of RMSE ###
    elif(printQuery=='histRMSE'):
        binList = np.linspace(0,1,21)
        print('bins',binList)

        #PLOT
        fig, (ax1,ax2,ax3,ax4,ax5,ax6) = plt.subplots(1,6, sharey=True)
        seaborn.histplot(ax=ax1, data=histRMSE[0], bins=binList, stat='count', color='green', edgecolor='black', linewidth=1)
        ax1.set_title('FBP', fontsize=15)
        ax1.set_ylabel('#Instances', fontsize=15)
        ax1.set_xlabel('RMSE', fontsize=15)
        print('FBP - binCount:',getCountInBins(ax1, binList), 'total:', len(histRMSE[0]))
        seaborn.histplot(ax=ax2, data=histRMSE[1], bins=binList, stat='count', color='black', edgecolor='black', linewidth=1)
        ax2.set_title('TRWBP', fontsize=15)
        ax2.set_xlabel('RMSE', fontsize=15)
        print('TRWBP - binCount:',getCountInBins(ax2, binList), 'total:', len(histRMSE[1]))
        seaborn.histplot(ax=ax3, data=histRMSE[2], bins=binList, stat='count', color='blue', edgecolor='black', linewidth=1)
        ax3.set_title('WMB', fontsize=15)
        ax3.set_xlabel('RMSE', fontsize=15)
        print('WMB - binCount:',getCountInBins(ax3, binList), 'total:', len(histRMSE[2]))
        seaborn.histplot(ax=ax4, data=histRMSE[3], bins=binList, stat='count', color='magenta', edgecolor='black', linewidth=1)
        ax4.set_title('IJGP', fontsize=15)
        ax4.set_xlabel('RMSE', fontsize=15)
        print('IJGP - binCount:',getCountInBins(ax4, binList), 'total:', len(histRMSE[3]))
        seaborn.histplot(ax=ax5, data=histRMSE[4], bins=binList, stat='count', color='deepskyblue', edgecolor='black', linewidth=1)
        ax5.set_title('GIBBS', fontsize=15)
        ax5.set_xlabel('RMSE', fontsize=15)
        print('Gibbs - binCount:',getCountInBins(ax5, binList), 'total:', len(histRMSE[4]))
        seaborn.histplot(ax=ax6, data=histRMSE[5], bins=binList, stat='count', color='red', edgecolor='black', linewidth=1)
        ax6.set_title('IBIA', fontsize=15)
        ax6.set_xlabel('RMSE', fontsize=15)
        print('IBIA - binCount:',getCountInBins(ax6, binList), 'total:', len(histRMSE[5]))
        plt.show()
    elif(printQuery == 'printErrMAR'):
        print('Benchmark,MaxErr(FBP), RMSE(FBP), Runtime(FBP), MaxErr(TRWBP), RMSE(TRWBP), Runtime(TRWBP), MaxErr(WMB), RMSE(WMB), Runtime(WMB),MaxErr(IJGP), RMSE(IJGP), Runtime(IJGP),MaxErr(Gibbs), RMSE(Gibbs), Runtime(Gibbs),MaxErr(IBIA), RMSE(IBIA), Runtime(IBIA)')
        print(printStr_MAR)
    elif(printQuery == 'printPR'):
        print('Benchmark,PR(ACE),PR(FBP),PR(TRWBP),PR(WMB),PR(IBIA)')
        print(printStr_PR)
    elif(printQuery == 'countWorkingTestcases'):
        print('#FBP', count_fbp)
        print('#TRWBP', count_trwbp)
        print('#WMB', count_wmb)
        print('#WMB10', count_wmb10)
        print('#IJGP', count_ijgp)
        print('#Gibbs', count_gibbs)
        print('#IBIA', count_ibia)


