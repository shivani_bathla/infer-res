def uaiRead(fileName, inferenceType):
    lineCount = 0
    numVar = 0
    evidVarList = []
    evidIncFlag = False
    with open(fileName, 'r') as fid:
        for line in fid:
            line = line.strip()
            if(line==""):
                continue
            lineCount += 1
            if(lineCount==2):
                continue
            elif(lineCount==3):
                parts = line.split()
                # check if variable.domainsize is 1
                l = [v for v in range(len(parts)) if(int(parts[v])==1)]
                if len(l):
                    if (inferenceType=='prior'):
                        print('Error: Priors cannot be inferred since domain-size of following variables is 1')
                        print(l)
                        sys.exit(1)
                    evidVarList = l
                    evidIncFlag = True
    
    return evidIncFlag, evidVarList

