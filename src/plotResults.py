from getData import getData

timeout=3600
inferenceType='posterior' # prior|posterior
benchmarkList=['BN_UAI']  # BN_UAI, mastermind, blockmap, fs, students, pedigree_uai, grid-BN, bnlearn

printQuery = 'countWorkingTestcases'
#printQuery = 'printErrMAR'
#printQuery = 'printPR'
#printQuery = 'score_vs_runtime'
#printQuery = 'histMaxError'
#printQuery = 'histRMSE'
#printQuery = 'histPR'
#printQuery = 'histMAR_aceNotWorking'

getData(timeout, benchmarkList, printQuery, inferenceType)
