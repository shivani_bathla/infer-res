import numpy as np
import os
import sys
import json
import math
from collections import defaultdict

def readIBIA(filePath):
    prFlag = False
    pr_runtime = -1.0
    total_runtime = -1.0
    marg_prop = defaultdict(list)
    with open(filePath, 'r') as fid:
        for line in fid:
            line = line.strip()
            if(line==""):
                continue
            elif(line=='PR'):
                prFlag = True
                continue
            elif(line=='MAR'):
                prFlag = False
                continue
            parts = line.split()
            if(prFlag):
                if (float(parts[0])==0.0):
                    logPR=parts[-1]
                else:
                    logPR = math.log(float(parts[0]),10)
            elif(parts[0]=='Runtime(PR)'):
                pr_runtime = parts[-1]
            elif(parts[0]=='Runtime(MAR)'):
                total_runtime = parts[-1]
            else:
                n = int(parts[0].rstrip(':'))
                margL = [float(val) for val in parts[1:]]
                marg_prop[n] = margL

    return marg_prop, logPR, pr_runtime, total_runtime

def readLibdaiMarginals(filePath, method):
    runtime = 10**6
    numIter = 1
    margDict = {}
    prEvid = 'nan'
    with open(filePath, 'r') as fid:
        for line in fid:
            line = line.lstrip('#')
            line = line.strip()
            if(line==""):
                continue
            parts = line.split()
            if(parts[0][0:2]=='PR'):
                if(len(parts)==2):
                    if(parts[1]!='-nan'):
                        # convert ln to log10
                        prEvid = float(parts[1])*math.log(math.e, 10)
                else:
                    if(parts[0][2:]!='-nan'):
                        # convert ln to log10
                        prEvid = float(parts[0][2:])*math.log(math.e, 10)
                continue
            if (parts[0]==method.upper()):
                runtime = float(parts[1])
                numIter = int(parts[2])
            elif(parts[0][0:3]=='({x'):
                parts[0] = parts[0].lstrip('({x')
                parts[0] = parts[0].rstrip('},')
                parts[1] = parts[1].lstrip('(')
                parts[-1] = parts[-1].rstrip('))')
                var = int(parts[0])
                margDict[var] = []

                for p in parts[1:]:
                    margDict[var].append(float(p.rstrip(',')))
                margDict[var] = np.array(margDict[var])
    return margDict, runtime, numIter, prEvid

def readAceMarginals(filePath):
    runtime = 0.0
    evidProb = -1.0
    margDict = {}
    with open(filePath, 'r') as fid:
        for line in fid:
            line = line.strip()
            if(line==""):
                continue
            parts = line.split()
            if (parts[0]=='Total'):
                # total initialization time + total inference time
                runtime += float(parts[-1])/1000
            elif(parts[0]=='Pr(e)'):
                evidProb = math.log(float(parts[-1]),10)
            elif(parts[0][0:4]=='Pr(x'):
                var = int(parts[0].rstrip(',e)').lstrip('Pr(x'))
                margDict[var]=[]
                parts[2] = parts[2].lstrip('[')
                parts[-1] = parts[-1].rstrip(']')
                for p in parts[2:]:
                    margDict[var].append(float(p.rstrip(',')))
                margDict[var] = np.array(margDict[var], dtype=np.float64)
    return margDict, runtime, evidProb

def readMAR(marFileName):
    i = 1
    var_indx = 0
    orgMargDict = {}
    marReadFlag=False
    prReadFlag = False
    statusReadFlag = False
    prVal =-1.0
    with open(marFileName, 'r') as fid: 
        for line in fid:
            line = line.strip()
            if (line==""):
                continue
            if (line=="MAR"):
                marReadFlag=True
                continue
            elif(line=="PR"):
                prReadFlag = True
                continue
            elif(line=="STATUS"):
                statusReadFlag = True
                continue
            if(prReadFlag):
                parts = line.split()
                # convert ln to log10
                prVal = float(parts[0])*math.log(math.e,10)
                prReadFlag = False
            elif(statusReadFlag):
                status = line.split(':')
                statusReadFlag = False
            elif(marReadFlag):
                parts = line.split()
                numVar = int(parts[0])
                l_indx = 1
                for var_indx in range(numVar):
                    domainSize = int(parts[l_indx])
                    orgMargDict[var_indx] = np.array([np.float64(parts[j]) for j in range(l_indx+1, l_indx+1+domainSize)], dtype=np.float64)
                    l_indx += (1+domainSize)
    return numVar, orgMargDict, prVal, status

